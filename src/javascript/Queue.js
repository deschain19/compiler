export class Queue
{

constructor(){
    this.values = {};
    this.front = 0;
    this.end = 0;
};

enqueue(data){
    this.values[this.end] = data;
    this.end++;
};

dequeue(){
    if(this.front==this.end){
        return null;
    };

    const data =  this.values[this.front];
    this.front++;

    return data;
    
};

peek(){
    if(this.size==0){
        return null;
    }
        return this.values[this.front];
    

};

size(){
    return  this.end-this.front;
};

empty(){
    if(this.size()){
        return null;
    }else{return false;}

};

print(){
    if(this.size()===0){return null}

    let result = '';
    for(let i = this.front;i<this.end;i++){
        result = this.values[i];
    }
    return result;
}




};