export class Stack
{

    constructor(){
        this.values = {};
        this.top = 0;

    };

    push(data){
        this.top++;
        this.values[this.top] = data;
    };

    pop(){
        let deleted;
        if(this.top){
            deleted = this.values[this.top];
            delete this.values[this.top];
            this.top--;
        
        };

        return deleted;

    };

    size(){
        return this.top;
    };

    empty(){
        if(!this.size()){
            return true;
        }else{return false;}
    };
    peek(){
        if(this.empty()){
            return null;
        }
        return this.values[this.top];
    };

  
}