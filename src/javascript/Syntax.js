import { Stack } from './Stack.js';
import { Queue } from './Queue.js';
import { sintacticMatrix,TR } from './Arrays.js';

export class Syntax{

    constructor(data,dire,line){
        this.data = data;
        this.dire = dire;
        this.line = line;
    }

    Analyisis(){

    var row = 0;
    var column = 0;
    var tok = 0;
    var tokens='';
    var items = [];
    var epsilon =false;
    var lineIndex=0;
    let queue = new Queue();
    let stack = new Stack();
    var sintacticError = false;
    stack.push(-129);
    stack.push(1);

    for(var int  = 0; int<this.dire;int++){

        queue.enqueue(this.data[int]);
        
    }
    queue.enqueue(-129);  
    
    do{

        if(stack.peek()>=500){
            alert("Error sintactico detectado "+queue.peek()+", en la linea");
            break;
        } 
        else if(queue.peek()==stack.peek()){

            console.log("Ha salido de la cola:" +queue.dequeue());
            console.log("Ha salido de la pila:" +stack.pop());

        }else if(epsilon){
            console.log("Ha salido de la pila epsilon:" +stack.pop());
            epsilon = false;

        }else{
            row = stack.peek();
            column = TR(queue.peek());
            if(row>0){
                tokens = sintacticMatrix(row,column);
                items = tokens.split(" ");
                if(items[0]==-130){
                    stack.pop();    
                }else{
                    if(stack.peek()!=-129){
                        console.log("Ha salido de la pila este valor para ingresar los siguientes:" +stack.pop());
                        for(var i =0;i<items.length;i++){
                            lineIndex++;
                            tok = items[i];
                            stack.push(parseInt(tok));
                            console.log("Ha ingresado a la pila:" +stack.peek());        
                       }
                    }                    
                }
            }else{alert("Error sintactico detectado, se esperaba: "+stack.peek()); break;}
        
        }
    }while(stack.peek()!=-129);

    }

    FillQueue(data){
    
        let tail = new Queue();
        tail.enqueue(data);

    }
}