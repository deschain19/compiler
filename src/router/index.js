import Vue from 'vue'
import Router from 'vue-router'
import Php from '@/components/Php'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'HelloWorld',
      component: Php
    }
  ]
})
